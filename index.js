const http = require("http");
const fs  = require("fs");

function rot13(s) {
    return s.toString().replace(/[A-Za-z]/g, function (c) {
      return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(
             "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm".indexOf(c)
      );
    } );
  }

const getRot13 = stringToRot => ({
    GivenString: `${stringToRot}`,
    rot13: rot13(stringToRot)
});


const requestHandler = (req, res) => {
    if (req.url === "/") {
        fs.readFile("views/index.html", "utf8", (err, html) => {
          if (err) throw err;
    res.writeHead(200, { "Content-Type": "text/html" });
          res.end(html);
        });
    }
    else if (req.url.startsWith("/api/rot13")) {
        const stringToRot = req.url.split("/api/rot13/");
        let rot13;
        if (isNaN(stringToRot)) {
            rot13 = getRot13(stringToRot)
        }
        else
            rot13 = {
                error: "invalid string, make sure there is not any number and the string isn't empty"
            };
        res.writeHead(200, { "content-type": "application/json" });
        res.end(JSON.stringify(rot13));
    }
};

const server = http.createServer(requestHandler);

server.listen(process.env.PORT || 4100, err => {
    if (err) throw err;


console.log(`Server running on PORT ${server.address().port}`);
});